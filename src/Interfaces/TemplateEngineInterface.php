<?php namespace Little\TemplateEngine\Interfaces ;
/**
 * TemplateEngine interface
 *
 * Provide methods for implementing a TemplateEngine
 *
 * @package Little\TemplateEngine
*/
interface TemplateEngineInterface {

    /**
     * Set the basic configuration for the Template Engine
     *
	 * @param array $templateEngineconfig
	 * @param \Little\Kore\Events $events
	 */
	public function __construct($templateEngineconfig,$events,\SimpleLog\Logger $logger);
    /**
     * add an inspected path to the template engine loader
     *
     * @param string $path
     * @param string $namespace
     */
	public function addPath($path,$namespace = null);
	/**
	 * Add a global variable (available in all templates)
	 *
	 * @param   string  $name   name of the variable
	 * @param   mixed   $value  the result to be print
	 */
	public function addGlobal($name, $value);
    /**
     * Add a function to the Template Engine
     *
     * @see http://www.sitepoint.com/extending-twig-templates-inheritance-filters-and-functions/
     * @param string    $name   name of the function
     * @param void      $data   can be an anonymous function or method
     * @param array     $args   optionals parametters
     *
    */
    public function addFunction($name, $data, $args = []);
    /**
     * Add a filter to the template engine
     *
     * @param   string  $name   Filter name
     * @param   void    $data   can be a method or anonymous function
     * @param   array   $args   optional arguments
    */
    public function addFilter($name, $data, $args = []);
	/**
	 * Render the given template
	 *
	 * @param   string  $template   name of the template
	 * @param   array   $data       optional data for the template
	 */
	public function render($template, $data = []);

}
