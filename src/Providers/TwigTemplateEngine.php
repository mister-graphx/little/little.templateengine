<?php namespace Little\TemplateEngine\Providers ;

use Little\TemplateEngine\Interfaces\TemplateEngineInterface as TemplateEngineInterface;
use SimpleLog\Logger ;
use Dflydev\DotAccessData\Data;

/**
* Twig implementation
*
* @package \Little\TemplateEngine\TwigTemplateEngine
*/
class TwigTemplateEngine implements TemplateEngineInterface {
    protected $events;
    protected $logger;
    protected $templateEngine;

    public  $config;

    protected $template_ext;

    public function __construct($twig_config,$events,\SimpleLog\Logger $logger){

        $this->config = new Data($twig_config);
        $this->events = $events;
        $this->logger  = $logger;

        if ($this->config->has('cache') == true || !is_dir($_ENV{'STORAGE_PATH'}.'cache/')) {
            $this->config->set('cache', $_ENV{'STORAGE_PATH'}.'cache/');
        }

        if($this->config->has('template_extension')){
            $this->template_ext = $this->config('template_extension');
        }else{
            $this->template_ext = ".twig";
        }

        // Loader : Dossier par défaut des templates PUBLIC_DIR.TEMPLATE_DIR
        $loader = new \Twig\Loader\FilesystemLoader(ROOT_PATH.'templates/');

        $this->templateEngine = new \Twig\Environment($loader, $this->config->export());
        // twig2 : load template_from_string extension
        // https://twig.symfony.com/doc/2.x/functions/template_from_string.html
        $this->templateEngine->addExtension(new \Twig\Extension\StringLoaderExtension());
        // Load debug dump extension
        // https://twig.symfony.com/doc/2.x/functions/dump.html
        if($this->config->get('debug') == true){
            $this->templateEngine->addExtension(new \Twig\Extension\DebugExtension());
        }

        // Config principale des dossier(s) inspectés par le moteur de template
        // ainsi que leur namespacing https://twig.symfony.com/doc/2.x/api.html
        // path : namespacing (__main__ !)
        foreach($this->config->get('templates_paths') as $path => $namespace){
            $this->addPath($path,$namespace);
        }

        $this->events->addListener('Kore.loaded',function($event,$container){
            $this->events->emit('Twig.loaded', $this);
            $this->logger->info('Twig : loaded', ['config'=>$this->config->export()]);
        });

        $this->events->addListener('Kore.onRun',function($event,$container){
            $this->events->emit('Twig.beforeGlobals', $this->templateEngine);
            // Load twigGlobals from twig.config
            if($this->config->has('globals') || is_array($this->config->get('globals'))){
                foreach($this->config->get('globals') as $global=>$value){
                    $this->templateEngine->addGlobal($global, $value);
                }
            }
            $this->logger->debug('Twig after globals', ['config'=>$this->config->export()]);
            $this->events->emit('Twig.afterGlobals', $this->templateEngine);
        });
    }
  /**
    * file template rendering method
    * @param  string   $template template file name
    * @param  array    $data     optional datas injected
    * @return string   html markup
    */
    public function render($template, $data = array()) {
        echo $this->templateEngine->render($template . $this->template_ext, $data);
    }
  /**
    * Render a string/inline template
    *
    * @param string $string inline template
    * @param array  $data   optional datas for the template
    * @return void
    * @todo ability to define namespaced template https://twig.symfony.com/doc/2.x/api.html
    */
    public function renderString($string, $data = array()) {
        $twig = clone $this->templateEngine;
        $template = $twig->createTemplate($string);
        return $this->templateEngine->render($template, $data);
    }
    /**
     * add an inspected path to the template engine loader
     *
     * @param string $path
     * @param string $namespace
     */
    public function addPath($path,$namespace = '__main__') {
        if (is_dir($path)) {
            ($namespace === null) ? $namespace = '__main__' : $namespace;

            $loader = $this->templateEngine->getLoader();
            $loader->addPath($path,$namespace);
            $this->templateEngine->setLoader($loader);
        }else{
            $this->logger->error('addPath : Is not a directory',['path'=>$path,'namespace'=> $namespace]);
        }
    }

    public function addGlobal($name, $value) {
        $this->templateEngine->addGlobal($name, $value);
    }

    public function addFunction($name, $data, $args = array()) {
        $function = new \Twig\TwigFunction($name, $data, $args);
        $this->templateEngine->addFunction($function);
    }

    public function addFilter($name, $data, $args = array()) {
        $filter = new \Twig\TwigFilter($name, $data, $args);
        $this->templateEngine->addFilter($filter);
    }
}
