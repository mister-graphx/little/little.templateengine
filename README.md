# Little Template Engine

Ce package propose une Interface permettant d'implémenter des moteurs de template utilisable avec Little.


## Providers

- twig Template engine

## Twig

### events

| Event | env |
|--|--|
`Twig.loaded` | `$this`
`Twig.beforeGlobals` | `Little\templateEngine`
`Twig.afterGlobals` | `Little\templateEngine`


### App Config


`config/app.yaml`

```yaml
parameters:
    twig_debug: false
    twig_cache: false
    twig_templates_dir: 'templates'
    twig_templates_paths:
        # path:namespace
        '%TEMPLATE_DIR%frontdoc': __main__
        '%TEMPLATE_DIR%default': __main__
    twig_globals:
        user_var: '1'
        user_var2: '2'

```
